"""Adding Query Model

Revision ID: f5c3c66c7953
Revises: 
Create Date: 2024-03-12 16:51:26.029439

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'f5c3c66c7953'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('queries',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('time', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.Column('wb_art_id', sa.Integer(), nullable=False),
    sa.Column('subscribed', sa.Boolean(), nullable=False),
    sa.Column('id', sa.BIGINT(), autoincrement=True, nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('queries')
    # ### end Alembic commands ###
