from aiogram import Bot, Dispatcher
from aiogram.fsm.storage.memory import MemoryStorage

from config import TELEGRAM_TOKEN

bot = Bot(token=TELEGRAM_TOKEN)

storage = MemoryStorage()

dp = Dispatcher(storage=storage)

