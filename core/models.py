from datetime import datetime

from sqlalchemy import create_engine, BIGINT, delete, Column, DateTime, func
from sqlalchemy.orm import DeclarativeBase, mapped_column, Mapped, Session

from config import DATABASE


engine = create_engine(
    DATABASE,
    isolation_level="READ UNCOMMITTED"
)


class Base(DeclarativeBase):
    id = Column(BIGINT, primary_key=True, autoincrement=True)

    def delete(self):
        with Session(engine) as ss:
            req = delete(type(self)).where(type(self).id == self.id)
            ss.execute(req)
            ss.commit()


class Query(Base):
    __tablename__ = 'queries'

    user_id: Mapped[int]
    time: Mapped[datetime] = mapped_column(DateTime(timezone=True), server_default=func.now())
    wb_art_id: Mapped[int]
    subscribed: Mapped[bool]
