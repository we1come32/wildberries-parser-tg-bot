SQLAlchemy~=2.0.25
aiogram~=3.3.0
alembic~=1.13.1
python-dotenv~=1.0.1
loguru~=0.7.2
psycopg2~=2.9.9
PyYAML~=6.0.1