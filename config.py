from os import getenv

from dotenv import load_dotenv

load_dotenv()

TELEGRAM_TOKEN = getenv('TOKEN')
DATABASE = getenv('DATABASE')
